# jekyll极简简历主题

#### 介绍
使用jekyll实现的极简风格简历主题,适配gitee page

[展示页](https://tqybt.gitee.io/minimalist_resume_cn/)

#### 软件架构
目录
```
├─assets    资产
│  ├─css    sass样式
│  ├─img    图片
│  └─js     JavaScript
├─_data     渲染数据
├─_includes html片段碎片
├─_layouts  布局样式文件
├─_sass     sass样式目录
└─_script   运行脚本目录
```
配置文件`_config.yml`发布配置
配置文件`_config-dev.yml`开发配置

#### 安装教程

1.  安装jekyll
2.  命令行运行`jekyll s -H 0.0.0.0 -P 80`
3.  访问[localhost/minimalist_resume_cn/](http://localhost/minimalist_resume_cn/)

#### 使用说明

###### 主题使用
1.  按照[jekyll使用主题](http://jekyllcn.com/docs/themes/)，所用主题名换为`minimalist_resume_cn-jekyll-theme`
2.  然后新建index.md内容如本项目内[index.html](./index.html),resume布局会自动进行渲染,修改_data/resume.yml为自定义内容即可

###### gitee page or github page使用

1. Fork 仓库
2. 然后新建index.md,使用`resume`布局
3. 需改[_config.yml](./_config.yml)中的baseurl为你的基础url
4. 开启gitee page
5. 修改_data/resume.yml为自定义内容
6. (可选)用markdown语法在*.md写页面文件,bootstrap布局和resume布局都支持[bootstrap4](https://v4.bootcss.com/docs/content/code/) html样式。可以在assets/css/stype.css写自己的样式,可以在assets/js/main.js写自己的脚本。
7. (可选)在根目录增加favicon.ico做网站图标

###### 其他说明

如果需要打印推荐使用chrome,可以在高级那儿取消页眉来不显示链接，其他的缩放自己调整一下

#### 使用和参考

1. [bootstrap](https://v4.bootcss.com/)
2. [jekyll](http://jekyllcn.com/)
3. [liquid模板语言](https://liquid.bootcss.com/)
4. 借鉴了[beautiful-jekyll](https://hub.fastgit.org/daattali/beautiful-jekyll.git)