---
layout: resume
---
[![Stargazers over time](https://whnb.wang/img/tqybt/minimalist_resume_cn){:height="50%" width="50%"}](https://whnb.wang/tqybt/minimalist_resume_cn){:target="_blank"}



<p class="text-center">版权 © 2021.<a href="{{site.gitee}}" target="_blank">Jieshao</a> 保留所有权利.
由<a href="https://jekyllrb.com/" target="_blank">jekyll</a>、<a href="v4.bootcss.com" target="_blank">bootstrapV4</a>提供渲染支持.以<a href="https://gitee.com/tqybt/minimalist-resume">jekyll极简风格</a>为基础.MIT LICENSE.版本 {{ 'now' | date: '%Y%m%d%H%M%S' }}</p>
