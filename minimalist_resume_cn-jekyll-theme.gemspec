# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "minimalist_resume_cn-jekyll-theme"
  spec.version       = "0.1.1"
  spec.authors       = ["huangjunjie"]
  spec.email         = ["1806620741@qq.com"]

  spec.homepage      = "http://tqybt.gitee.io/minimalist_resume_cn/"
  spec.summary       = "极简简历风格"
  spec.description   = "极简简历是一个简单的简历渲染模板."

  spec.metadata      = {
    "source_code_uri" => "https://gitee.com/tqybt/minimalist_resume_cn",
    "bug_tracker_uri" => "https://gitee.com/tqybt/minimalist_resume_cn/issues",
    "changelog_uri"   => "https://gitee.com/tqybt/minimalist_resume_cn/releases",
    "homepage_uri"    => spec.homepage,
  }

  spec.extra_rdoc_files = %w(README.md LICENSE)

  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.2"
end
